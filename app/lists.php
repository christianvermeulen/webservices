<?php
/**
 * Show a list of several possible resources
 *  
 * @author     Christian Vermeulen <info@christianvermeulen.net>
 * @copyright  2012 Christian Vermeulen
 */

// return a list of cities
function listCities()
{
	$db = connectDB();
	$app = Slim::getInstance();

	$res = $db->query("SELECT city FROM hotels GROUP BY city");
	if($res->num_rows > 0)
	{
		$cities = array();
		while($city = $res->fetch_assoc())
			$cities['cities'][] = $city['city'];

		reply( $cities );
	}
}

// return a list of states
function listStates()
{
	$db = connectDB();
	$app = Slim::getInstance();

	$res = $db->query("SELECT state FROM hotels GROUP BY state");
	if($res->num_rows > 0)
	{
		$states = array();
		while($state = $res->fetch_assoc())
			$states['states'][] = $state['state'];

		reply( $states );
	}
}

//return a list of counties
function listCounties()
{
	$db = connectDB();
	$app = Slim::getInstance();

	$res = $db->query("SELECT county FROM hotels GROUP BY county");
	if($res->num_rows > 0)
	{
		$counties = array();
		while($county = $res->fetch_assoc())
			$counties['counties'][] = $county['county'];

		reply( $counties );
	}
}