<?php
/**
 * Handle the landing page
 *  
 * @author     Christian Vermeulen <info@christianvermeulen.net>
 * @copyright  2012 Christian Vermeulen
 */

include "app/atx.php";
include "app/utils.php";
include "app/hotels.php";
include "app/lists.php";

function home()
{
	$app = Slim::getInstance();
	switch($app->request()->getMethod())
	{
		case "PUT":
			create();
			break;
		case "GET":
		default:
			echo "<h1>Hotels around America</h1>";
			echo "<p>Please read the documentation to figure out how use this api</p>";
			break;
	}
}