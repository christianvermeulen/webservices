<?php
/**
 * Main function for responses
 *  
 * @author     Christian Vermeulen <info@christianvermeulen.net>
 * @copyright  2012 Christian Vermeulen
 */

/******************
*	All functions for replies
*******************/

// Connect to db
function connectDB()
{
	$db = new mysqli("localhost","hrprj_str","str","hrprj_str");
	return $db;
}

// Handle reply
function reply($data)
{
	// Check if a type is requested
	isset($_GET['type']) ? $type = $_GET['type'] : $type = "";

	// Choose the right response
	switch($type)
	{
		case "xml":
			echo toXML( $data );
			break;
		case "html":
			echo toHTML( $data );
			break;
		case "json":
		default:
			echo toJson( $data );
			break;
	}
}

// Return data in JSON
function toJson($data)
{
	$response = json_encode($data);
	return $response;
}

// Return data in XML
function toXML($data)
{
	echo ArrayToXML::toXml($data,"root");
}

// function defination to convert array to xml
function array_to_xml($student_info, &$xml_student_info) {
    
}

// Return in HTML
function toHTML($data)
{
	echo "<pre>";
	print_r($data);
	echo "</pre>";
}

// Check api key
function checkKey()
{
	$valid = "15fb4452461421dbdeb9222066f19195be0bd1cddec75f48d47612ccdd83a377";
	$app = Slim::getInstance();
	$key = $app->request()->get('key');
	if($key == $valid)
		return true;
	else
		return false;
}