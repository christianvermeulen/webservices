<?php

/**
 * All the function for retrieving data
 *  
 * @author     Christian Vermeulen <info@christianvermeulen.net>
 * @copyright  2012 Christian Vermeulen
 */

// Find hotels by city
function findByCity($city)
{
	// Connect to db
	$db = connectDB();
	
	// Set query
	$res = $db->query("SELECT * FROM hotels WHERE city = '".$city."'");

	// Check if hotel is found
	if($res->num_rows > 0)
	{
		$hotels = array();
		while($hotel = $res->fetch_assoc())
			$hotels["hotels"][] = $hotel;

		reply( $hotels );
	}
	else
	{
		// hotel not found
		$app = Slim::getInstance();
		$app->response()->status(404);
	}
}

// Find hotels by state
function findByState($state)
{
	// Connect to db
	$db = connectDB();
	
	// Set query
	$res = $db->query("SELECT * FROM hotels WHERE state = '".$state."'");

	// Check if hotel is found
	if($res->num_rows > 0)
	{
		$hotels = array();
		while($hotel = $res->fetch_assoc())
			$hotels["hotels"][] = $hotel;

		reply( $hotels );
	}
	else
	{
		// hotel not found
		$app = Slim::getInstance();
		$app->response()->status(404);
	}
}

// Find hotels by county
function findByCounty($county)
{
	// Connect to db
	$db = connectDB();
	
	// Set query
	$res = $db->query("SELECT * FROM hotels WHERE county = '".$county."'");

	// Check if hotel is found
	if($res->num_rows > 0)
	{
		$hotels = array();
		while($hotel = $res->fetch_assoc())
			$hotels["hotels"][] = $hotel;

		reply( $hotels );
	}
	else
	{
		// hotel not found
		$app = Slim::getInstance();
		$app->response()->status(404);
	}
}

// Find hotels by search
function findBySearch($q)
{
	// Connect to db
	$db = connectDB();
	
	// Set query
	$res = $db->query("SELECT * FROM hotels WHERE name LIKE '%".urldecode($q)."%'");

	// Check if hotel is found
	if($res->num_rows > 0)
	{
		$hotels = array();
		while($hotel = $res->fetch_assoc())
			$hotels["hotels"][] = $hotel;

		reply( $hotels );
	}
	else
	{
		// hotel not found
		$app = Slim::getInstance();
		$app->response()->status(404);
	}
}

// Find hotel by ID
function singleHotel($id)
{
	$app = Slim::getInstance();

	switch($app->request()->getMethod())
	{
		case "GET":
			findById($id);
			break;
		case "PUT":
			updateHotel($id);
			break;
		case "DELETE":
			deleteHotel($id);
			break;
		default:
			$app->response()->status(404);
			break;
	}
}

// Find a hotel by ID
function findById($id)
{
	// Connect to db
	$db = connectDB();
	
	// Set query
	$res = $db->query("SELECT * FROM hotels WHERE id = '".$id."'");

	// Check if hotel is found
	if($res->num_rows == 1)
	{	
		$data = array(
			"hotels" => array($res->fetch_assoc())
		);
		reply( $data );
	}
	else
	{
		// hotel not found
		$app = Slim::getInstance();
		$app->response()->status(404);
	}
}

// Update a hotel
function updateHotel($id)
{
	$app = Slim::getInstance();
	
	if( checkKey() )
	{		
		$new = $app->request()->put();
		
		$db = connectDB();
		$db->autocommit(FALSE);

		if( isset($new['name']) )
			$db->query("UPDATE hotels SET name = '".addslashes($new['name'])."' WHERE id = ".$id);
		if( isset($new['address']) )
			$db->query("UPDATE hotels SET address = '".addslashes($new['address'])."' WHERE id = ".$id);
		if( isset($new['city']) )
			$db->query("UPDATE hotels SET city = '".addslashes($new['city'])."' WHERE id = ".$id);
		if( isset($new['state']) )
			$db->query("UPDATE hotels SET state = '".addslashes($new['state'])."' WHERE id = ".$id);
		if( isset($new['zip']) )
			$db->query("UPDATE hotels SET zip = '".addslashes($new['zip'])."' WHERE id = ".$id);
		if( isset($new['phone']) )
			$db->query("UPDATE hotels SET phone = '".addslashes($new['phone'])."' WHERE id = ".$id);
		if( isset($new['fax']) )
			$db->query("UPDATE hotels SET fax = '".addslashes($new['fax'])."' WHERE id = ".$id);
		if( isset($new['email']) )
			$db->query("UPDATE hotels SET email = '".addslashes($new['email'])."' WHERE id = ".$id);
		if( isset($new['url']) )
			$db->query("UPDATE hotels SET url = '".addslashes($new['url'])."' WHERE id = ".$id);
		if( isset($new['areacode']) )
			$db->query("UPDATE hotels SET areacode = '".addslashes($new['areacode'])."' WHERE id = ".$id);
		if( isset($new['county']) )
			$db->query("UPDATE hotels SET county = '".addslashes($new['county'])."' WHERE id = ".$id);

		if(!$db->commit())
		{
			$app->response()->status(400);
		}
	}
	else
	{
		$app->response()->status(401);
	}
}


// Create a new hotel!
function create()
{
	$app = Slim::getInstance();
	if( checkKey() )
	{
		$new = $app->request()->put();
		$db = connectDB();

		if( isset($new['name']) && 
			isset($new['address']) && 
			isset($new['city']) && 
			isset($new['state']) && 
			isset($new['zip']) && 
			isset($new['phone']) && 
			isset($new['fax']) && 
			isset($new['email']) && 
			isset($new['url']) && 
			isset($new['areacode']) && 
			isset($new['county']) )
		{
			$query = "INSERT INTO hotels (name, address, city, state, zip, phone, fax, email, url, areacode, county) VALUES (
				'".$new['name']."',
				'".$new['address']."',
				'".$new['city']."',
				'".$new['state']."',
				'".$new['zip']."',
				'".$new['phone']."',
				'".$new['fax']."',
				'".$new['email']."',
				'".$new['url']."',
				'".$new['areacode']."',
				'".$new['county']."'
				)";
			if(!$db->query($query))
			{
				$app->response()->body($db->mysql_error());
				$app->response()->status(500);
			}
			else
			{
				echo $db->insert_id;
			}
		}
		else
		{
			$app->response()->body("missing parameters");
			$app->response()->status(400);
		}
	}
	else
	{
		$app->response()->body("invalid api key");
		$app->response()->status(401);
	}
}

// Delete a hotel
function deleteHotel($id)
{
	$app = Slim::getInstance();
	if( checkKey() )
	{	
		$db = connectDB();
		$db->autocommit(false);
		$db->query("DELETE FROM hotels WHERE id = ".$id);
		if(!$db->commit())
		{
			$app->response()->status(400);
		}
	}
	else
	{
		$app->response()->status(401);
	}
}