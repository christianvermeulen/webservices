<?php
/**
 * Step 1: Require the Slim PHP 5 Framework
 *
 * If using the default file layout, the `Slim/` directory
 * will already be on your include path. If you move the `Slim/`
 * directory elsewhere, ensure that it is added to your include path
 * or update this file path as needed.
 */

require 'Slim/Slim.php';

/**
 * Step 2: Instantiate the Slim application
 *
 * Here we instantiate the Slim application with its default settings.
 * However, we could also pass a key-value array of settings.
 * Refer to the online documentation for available settings.
 */
$app = new Slim();
$app->config('debug', true);
$app->config('log.enabled', true);
$app->config('log.level', 4);

/**
 * Step 3: Define the Slim application routes
 *
 * Here we define several Slim application routes that respond
 * to appropriate HTTP request methods. In this example, the second
 * argument for `Slim::get`, `Slim::post`, `Slim::put`, and `Slim::delete`
 * is an anonymous function. If you are using PHP < 5.3, the
 * second argument should be any variable that returns `true` for
 * `is_callable()`. An example GET route for PHP < 5.3 is:
 *
 * $app = new Slim();
 * $app->get('/hello/:name', 'myFunction');
 * function myFunction($name) { echo "Hello, $name"; }
 *
 * The routes below work with PHP >= 5.3.
 */


//GET routes
$app->get('/','root');
function root()
{
	echo "<h1>Welcome</h1>";
	echo "<p>Please read the <a href='documentation.doc'>documentation</a>.</p>";
}
$app->get('/hotels/city/:city', 'findByCity');
$app->get('/hotels/state/:state', 'findByState');
$app->get('/hotels/county/:county', 'findByCounty');
$app->get('/hotels/search/:q', 'findBySearch');
$app->get('/cities', 'listCities');
$app->get('/states', 'listStates');
$app->get('/counties', 'listCounties');

// PUT routes
$app->map('/hotels', 'home')->via('GET','PUT');

// Handle single hotel requests
$app->map('/hotels/:id', 'singleHotel')->via('GET','PUT','DELETE');

include "app/index.php";

/**
 * Step 4: Run the Slim application
 *
 * This method should be called last. This is responsible for executing
 * the Slim application using the settings and routes defined above.
 */
$app->run();